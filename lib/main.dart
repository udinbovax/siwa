import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:siwa/core/app_widget.dart';
import 'package:siwa/injection.dart';

void main() async {
  await configureInjection(Environment.dev);
  runApp(AppWidget());
}