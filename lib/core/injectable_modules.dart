import 'package:injectable/injectable.dart';
import 'package:siwa/router/auth_guards.dart';
import 'package:siwa/router/router.gr.dart';


@module
abstract class InjectableModule {
  @lazySingleton
  AppRouter get router => AppRouter(authGuard: AuthGuard());
}
