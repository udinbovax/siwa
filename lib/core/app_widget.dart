
import 'package:flutter/material.dart';
import 'package:siwa/injection.dart';
import 'package:siwa/router/router.gr.dart';

class AppWidget extends StatelessWidget {
  final _appRouter = getIt<AppRouter>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'Siwa Assignment',
      debugShowCheckedModeBanner: false,
      theme: ThemeData.light().copyWith(
          inputDecorationTheme: InputDecorationTheme(border: InputBorder.none)),
      routerDelegate: _appRouter.delegate(),
      routeInformationParser: _appRouter.defaultRouteParser(),
    );
  }
}
