// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

import 'package:auto_route/auto_route.dart' as _i3;
import 'package:flutter/material.dart' as _i4;

import '../model/movies.dart' as _i6;
import '../views/edit_movie_page.dart' as _i2;
import '../views/home_page.dart' as _i1;
import 'auth_guards.dart' as _i5;

class AppRouter extends _i3.RootStackRouter {
  AppRouter(
      {_i4.GlobalKey<_i4.NavigatorState>? navigatorKey,
      required this.authGuard})
      : super(navigatorKey);

  final _i5.AuthGuard authGuard;

  @override
  final Map<String, _i3.PageFactory> pagesMap = {
    HomePageRoute.name: (routeData) {
      final args = routeData.argsAs<HomePageRouteArgs>();
      return _i3.MaterialPageX<dynamic>(
          routeData: routeData,
          child: _i1.HomePage(key: args.key, title: args.title));
    },
    EditMoviePageRoute.name: (routeData) {
      final args = routeData.argsAs<EditMoviePageRouteArgs>();
      return _i3.MaterialPageX<dynamic>(
          routeData: routeData,
          child: _i2.EditMoviePage(
              key: args.key,
              title: args.title,
              type: args.type,
              movies: args.movies));
    }
  };

  @override
  List<_i3.RouteConfig> get routes => [
        _i3.RouteConfig(HomePageRoute.name, path: '/'),
        _i3.RouteConfig(EditMoviePageRoute.name,
            path: '/edit-movie-page', guards: [authGuard])
      ];
}

/// generated route for
/// [_i1.HomePage]
class HomePageRoute extends _i3.PageRouteInfo<HomePageRouteArgs> {
  HomePageRoute({_i4.Key? key, required String title})
      : super(HomePageRoute.name,
            path: '/', args: HomePageRouteArgs(key: key, title: title));

  static const String name = 'HomePageRoute';
}

class HomePageRouteArgs {
  const HomePageRouteArgs({this.key, required this.title});

  final _i4.Key? key;

  final String title;

  @override
  String toString() {
    return 'HomePageRouteArgs{key: $key, title: $title}';
  }
}

/// generated route for
/// [_i2.EditMoviePage]
class EditMoviePageRoute extends _i3.PageRouteInfo<EditMoviePageRouteArgs> {
  EditMoviePageRoute(
      {_i4.Key? key,
      required String title,
      required int type,
      _i6.Movies? movies})
      : super(EditMoviePageRoute.name,
            path: '/edit-movie-page',
            args: EditMoviePageRouteArgs(
                key: key, title: title, type: type, movies: movies));

  static const String name = 'EditMoviePageRoute';
}

class EditMoviePageRouteArgs {
  const EditMoviePageRouteArgs(
      {this.key, required this.title, required this.type, this.movies});

  final _i4.Key? key;

  final String title;

  final int type;

  final _i6.Movies? movies;

  @override
  String toString() {
    return 'EditMoviePageRouteArgs{key: $key, title: $title, type: $type, movies: $movies}';
  }
}
