import 'package:auto_route/auto_route.dart';
import 'package:siwa/router/auth_guards.dart';
import 'package:siwa/views/edit_movie_page.dart';
import 'package:siwa/views/home_page.dart';
import 'package:auto_route/annotations.dart';

@MaterialAutoRouter(
  routes: <AutoRoute>[
    AutoRoute(page: HomePage, initial: true),
    AutoRoute(page: EditMoviePage, guards: [AuthGuard]),
  ],
)
class $AppRouter {}
